package net.pyj246.webbase.locale;

import net.pyj246.webbase.model.base.MessagePack;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.support.AbstractMessageSource;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Created by pyj246 on 2015. 10. 27..
 */
public class DatabaseMessageSource extends AbstractMessageSource implements InitializingBean {

    private final Map<String, MessageFormat> messageFormatMap = new HashMap<>();

    private final Map<String, MessagePack> messagePackMap = new HashMap<>();

    @Override
    protected MessageFormat resolveCode(String code, Locale locale) {
        MessageFormat messageFormat = null;
        String message = resolveCodeWithoutArguments(code, locale);

        if (message != null) {
            String formatKey = getMessageFormatKey(code, locale);
            synchronized (messageFormatMap) {
                messageFormat = this.messageFormatMap.get(formatKey);
                if (messageFormat == null) {
                    messageFormat = createMessageFormat(message, locale);
                    this.messageFormatMap.put(formatKey, messageFormat);
                }
            }
        }

        return messageFormat;
    }

    @Override
    protected String resolveCodeWithoutArguments(String code, Locale locale) {
        String message = null;

        if (messagePackMap.containsKey(locale.getLanguage())) {
            synchronized (messagePackMap) {
                MessagePack messagePack = messagePackMap.get(locale.getLanguage());
                if (!messagePack.getMessages().containsKey(code)) {
                    // TODO 리로드 로직 추가
                }
                message = messagePack.getMessages().get(code);
            }
        }

        return message;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        messagePackMap.clear();

    }

    private void loadMessagePack(Locale locale) {
        MessagePack messagePack = null;

        messagePackMap.put(locale.getLanguage(), messagePack);
    }

    private String getMessageFormatKey(String code, Locale locale) {
        return code.concat(locale.getLanguage());
    }
}
