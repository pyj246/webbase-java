package net.pyj246.webbase.model.base;

import java.util.Map;

/**
 * Created by pyj246 on 2015. 10. 27..
 */
public class MessagePack {

    private String language;

    private Map<String, String> messages;

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public Map<String, String> getMessages() {
        return messages;
    }

    public void setMessages(Map<String, String> messages) {
        this.messages = messages;
    }

}
